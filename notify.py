from gi.repository import GLib
import dbus
from dbus.mainloop.glib import DBusGMainLoop
import os
import telebot # pyTelegramBotAPI

class Listener:
    def __init__(self, bot):
        self.exclude = ["vpnui", "xfce4-power-manager"]
        self.last_hash = None
        self.bot = bot
        self.chat_id = 341191072
        try:
            self.bot.send_message(self.chat_id, "Up and running.")
        except Exception as e:
            print('Send failed: {}'.format(str(e)))

    def __call__(self, *callargs, **kwargs): #bus, message
        try:
            if len(callargs) == 1:
                print('Short callargs: {}'.format(callargs))
                return
            args = callargs[1].get_args_list()
            if len(args) < 5:
                print('Short args: {}'.format(args))
                return
            out = 'From: {}, Summary: {}, Text: {}'.format(args[0], args[3], args[4])
            if args[0] in self.exclude:
                print('Excluded: {}'.format(out))
                return
            this_hash = hash(out)
            if this_hash == self.last_hash: # We seem to get notifications from dbus twice
                return
            self.last_hash = this_hash
            print('Send to {}: {}'.format(self.chat_id, out))
            self.bot.send_message(self.chat_id, out)
        except Exception as e: # If any exceptions escape, it breaks dbus
            print('Err: {}, {}'.format(str(e), args))


DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()
bus.add_match_string("eavesdrop=true, interface='org.freedesktop.Notifications', member='Notify'")

bot = telebot.TeleBot("YOUR_BOT_KEY", parse_mode=None)
@bot.message_handler(commands=['ping'])
def handle_ping(message):
    print('Got ping, responding...')
    bot.reply_to(message, "Running")
#bot.polling(timeout=1)
bus.add_message_filter(Listener(bot))

mainloop = GLib.MainLoop()
mainloop.run()
